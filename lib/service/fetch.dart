import 'package:http/http.dart' as http;
import 'package:meta/meta_meta.dart';
import '../model/post.dart';

class Fetch {
  Future<List<Welcome>?> getPosts() async {
    var client = http.Client();

    var uri = Uri.parse('https://jsonplaceholder.typicode.com/posts');
    var respone = await client.get(uri);
    if (respone.statusCode == 200) {
      var json = respone.body;
      return welcomeFromJson(json);
    }
  }
}
