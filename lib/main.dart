import 'package:api_flutter/model/post.dart';
import 'package:api_flutter/service/fetch.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MaterialApp(home: const MyApp()));
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
          // This is the theme of your application.
          //
          // Try running your application with "flutter run". You'll see the
          // application has a blue toolbar. Then, without quitting the app, try
          // changing the primarySwatch below to Colors.green and then invoke
          // "hot reload" (press "r" in the console where you ran "flutter run",
          // or simply save your changes to "hot reload" in a Flutter IDE).
          // Notice that the counter didn't reset back to zero; the application
          // is not restarted.
          primarySwatch: Colors.blue,
        ),
        home: const MyHomePage());
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key});

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  List<Welcome>? welcome;
  var isLoaded = false;
  @override
  void initState() {
    super.initState();

    getData();
  }

  getData() async {
    welcome = await Fetch().getPosts();
    if (welcome != null) {
      setState(() {
        isLoaded = true;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text("Api fecth test")),
        body: ListView.builder(
            itemCount: welcome?.length,
            itemBuilder: (context, index) {
              return Padding(
                  padding: EdgeInsets.all(10),
                  child: (Container(
                      child: Column(children: [
                    Text(welcome![index].title,
                        style: TextStyle(
                          fontWeight: FontWeight.w700,
                        )),
                    Text(welcome?[index].body ?? '')
                  ]))));
            }));
  }
}
